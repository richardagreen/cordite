/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.test.utils

import net.corda.core.identity.CordaX500Name
import net.corda.core.identity.Party
import net.corda.testing.driver.PortAllocation
import net.corda.testing.node.StartedMockNode

class BraidPortHelper(private val portAllocation: PortAllocation = defaultPortAllocator) {
  companion object {
    val defaultPortAllocator = PortAllocation.Incremental(8081)
  }

  val portMap = mutableMapOf<String, Int>()

  fun portForNode(node: StartedMockNode) : Int {
    return portForParty(node.info.legalIdentities.first())
  }

  fun portForParty(party: Party) : Int {
    return portForName(party.name.organisation)
  }

  fun portForName(name: String): Int {
    return portMap.computeIfAbsent(name) {
      val org = it.replace(" ", "")
      val port = portAllocation.nextPort()
      System.setProperty("braid.$org.port", port.toString())
      port
    }
  }

  fun setSystemPropertiesFor(vararg names: CordaX500Name) {
    names.forEach { portForName(it.organisation) }
  }
}
