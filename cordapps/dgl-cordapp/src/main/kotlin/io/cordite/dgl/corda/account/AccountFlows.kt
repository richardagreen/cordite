/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dgl.corda.account

import co.paralleluniverse.fibers.Suspendable
import io.cordite.dgl.corda.crud.CrudCreateFlow
import io.cordite.dgl.corda.crud.CrudUpdateFlow
import io.cordite.dgl.corda.tag.Tag
import io.cordite.dgl.corda.tag.WellKnownTagCategories
import net.corda.core.contracts.StateAndRef
import net.corda.core.flows.FlowLogic
import net.corda.core.flows.InitiatingFlow
import net.corda.core.flows.StartableByRPC
import net.corda.core.flows.StartableByService
import net.corda.core.identity.Party
import net.corda.core.node.services.queryBy
import net.corda.core.node.services.vault.*
import net.corda.core.serialization.CordaSerializable
import net.corda.core.transactions.SignedTransaction
import net.corda.core.utilities.loggerFor
import org.crsh.cli.impl.descriptor.IllegalParameterException

@InitiatingFlow
@StartableByRPC
@StartableByService
class CreateAccountFlow(
    private val requests: List<Request>,
    private val notary: Party
) : FlowLogic<SignedTransaction>() {
  @Suspendable
  @Throws(CreateAccountFlow.Exception::class)
  override fun call(): SignedTransaction {
    if (requests.isEmpty()) {
      throw Exception("there must be at least one account for ${CreateAccountFlow::class.simpleName}")
    }
    val states = requests.map { request ->
      val accountAddress = AccountAddress.create(request.accountId, ourIdentity.name)
      Account.State(address = accountAddress, participants = listOf(ourIdentity), tags = setOf(Tag(WellKnownTagCategories.DGL_ID, accountAddress.toString())))
    }

    return subFlow(CrudCreateFlow(Account.State::class.java, states, Account.CONTRACT_ID, notary)).apply {
      logger.info("create accounts - ${requests.joinToString(separator = ",") { it.accountId }}")
    }
  }

  @CordaSerializable
  data class Request(val accountId: String)

  class Exception(msg: String) : RuntimeException(msg)
}

@InitiatingFlow
@StartableByRPC
@StartableByService
class SetAccountTagFlow(
    private val accountId: String,
    private val tag: Tag,
    private val notary: Party
) : FlowLogic<SignedTransaction>() {
  @Suspendable
  override fun call(): SignedTransaction {
    val accountStateRef = subFlow(GetAccountFlow(accountId))
    val account = accountStateRef.state.data
    val newAliases = account.tags
        .associateBy { it.category }
        .toMutableMap()
        .apply { put(tag.category, tag) }
        .values.toSet()
    val updatedAccount = account.copy(tags = newAliases)
    return subFlow(CrudUpdateFlow(listOf(accountStateRef), listOf(updatedAccount), Account.CONTRACT_ID, notary))
  }
}

@InitiatingFlow
@StartableByRPC
@StartableByService
class RemoveAccountTagFlow(
    private val accountId: String,
    private val category: String,
    private val notary: Party
) : FlowLogic<SignedTransaction>() {
  @Suspendable
  override fun call(): SignedTransaction {
    val accountStateRef = subFlow(GetAccountFlow(accountId))
    val account = accountStateRef.state.data
    val newAliases = account.tags.associateBy { it.category }
        .toMutableMap()
        .apply { remove(category) }
        .values.toSet()
    val updatedAccount = account.copy(tags = newAliases)
    return subFlow(CrudUpdateFlow(listOf(accountStateRef), listOf(updatedAccount), Account.CONTRACT_ID, notary))
  }
}

@InitiatingFlow
@StartableByRPC
@StartableByService
class GetAccountFlow(private val accountId: String) : FlowLogic<StateAndRef<Account.State>>() {
  @Suspendable
  override fun call(): StateAndRef<Account.State> {
    val result = serviceHub.vaultService.queryBy(Account.State::class.java, builder {
      QueryCriteria.VaultCustomQueryCriteria(Account.AccountSchemaV1.PersistentAccount::accountId.equal(accountId))
    })
    if (result.states.isEmpty()) throw IllegalParameterException("cannot find account $accountId")
    if (result.states.count() > 1) throw IllegalStateException("multiple accounts for $accountId!!")
    return result.states.first()
  }
}

@InitiatingFlow
@StartableByRPC
@StartableByService
class FindAccountsFlow(private val tag: Tag,
                       private val pageNumber: Int = DEFAULT_PAGE_NUM,
                       private val pageSize: Int = DEFAULT_PAGE_SIZE) : FlowLogic<Set<Account.State>>() {
  @Suspendable
  override fun call(): Set<Account.State> {
    return Account.findAccountsWithTag(serviceHub, tag, PageSpecification(pageNumber, pageSize))
  }
}

@InitiatingFlow
@StartableByRPC
@StartableByService
class ListAllAccountsFlow(private val page: Int = DEFAULT_PAGE_NUM,
                          private val pageSize: Int = DEFAULT_PAGE_SIZE) : FlowLogic<List<Account.State>>() {
  companion object {
    val log = loggerFor<ListAllAccountsFlow>()
  }
  @Suspendable
  override fun call(): List<Account.State> {
    log.trace("ListAllAccountsFlow")
    val paging = PageSpecification(page, pageSize)
    val qc = QueryCriteria.VaultQueryCriteria(contractStateTypes = setOf(Account.State::class.java))
    return serviceHub.vaultService.queryBy<Account.State>(qc, paging).states.map { it.state.data }
  }
}