/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dgl.corda.token

//import co.paralleluniverse.fibers.Suspendable
//import net.corda.core.contracts.Amount
//import net.corda.core.contracts.StateAndRef
//import net.corda.core.contracts.StateRef
//import net.corda.core.contracts.TransactionState
//import net.corda.core.crypto.SecureHash
//import net.corda.core.flows.FlowLogic
//import net.corda.core.identity.AbstractParty
//import net.corda.core.identity.Party
//import net.corda.core.node.ServiceHub
//import net.corda.core.node.services.StatesNotAvailableException
//import net.corda.core.serialization.SerializationDefaults
//import net.corda.core.serialization.deserialize
//import net.corda.core.utilities.*
//import java.sql.SQLException
//import java.util.*
//import java.util.concurrent.locks.ReentrantLock
//import kotlin.concurrent.withLock
//
//class TokenSelection {
//  companion object {
//    val log = loggerFor<TokenSelection>()
//  }
//
//  // coin selection retry loop counter, sleep (msecs) and lock for selecting states
//  private val MAX_RETRIES = 8
//  private val RETRY_SLEEP = 100
//  private val RETRY_CAP = 2000
//  private val spendLock: ReentrantLock = ReentrantLock()
//
//  @Suspendable
//  fun unconsumedCashStatesForSpending(services: ServiceHub,
//                                      accountId: String,
//                                      amount: Amount<TokenType.Descriptor>,
//                                      onlyFromIssuerParties: Set<AbstractParty> = emptySet(),
//                                      notary: Party? = null,
//                                      lockId: UUID,
//                                      withIssuerRefs: Set<OpaqueBytes> = emptySet()): List<StateAndRef<Token.State>> {
//    val issuerKeysStr = onlyFromIssuerParties.fold("") { left, right -> left + "('${right.owningKey.toBase58String()}')," }.dropLast(1)
//    val issuerRefsStr = withIssuerRefs.fold("") { left, right -> left + "('${right.bytes.toHexString()}')," }.dropLast(1)
//
//    val stateAndRefs = mutableListOf<StateAndRef<Token.State>>()
//
//    //       We are using an H2 specific means of selecting a minimum set of rows that match a request amount of coins:
//    //       1) There is no standard SQL mechanism of calculating a cumulative total on a field and restricting row selection on the
//    //          running total of such an accumulator
//    //       2) H2 uses session variables to perform this accumulator function:
//    //          http://www.h2database.com/html/functions.html#set
//    //       3) H2 does not support JOIN's in FOR UPDATE (hence we are forced to execute 2 queries)
//
//    for (retryCount in 1..MAX_RETRIES) {
//      if (!attemptSpend(services, accountId, amount, lockId, notary, onlyFromIssuerParties, issuerKeysStr, withIssuerRefs, issuerRefsStr, stateAndRefs)) {
//        log.warn("Coin selection failed on attempt $retryCount")
//        // TODO: revisit the back off strategy for contended spending. https://gitlab.com/cordite/cordite/issues/294
//        if (retryCount != MAX_RETRIES) {
//          stateAndRefs.clear()
//          val durationMillis = (minOf(RETRY_SLEEP.shl(retryCount), RETRY_CAP / 2) * (1.0 + Math.random())).toInt()
//          FlowLogic.sleep(durationMillis.millis)
//        } else {
//          log.warn("Insufficient spendable states identified for $amount")
//        }
//      } else {
//        break
//      }
//    }
//    return stateAndRefs
//  }
//
//  private fun attemptSpend(
//      services: ServiceHub,
//      accountId: String,
//      amount: Amount<TokenType.Descriptor>,
//      lockId: UUID,
//      notary: Party?,
//      onlyFromIssuerParties: Set<AbstractParty>,
//      issuerKeysStr: String,
//      withIssuerRefs: Set<OpaqueBytes>,
//      issuerRefsStr: String,
//      stateAndRefs: MutableList<StateAndRef<Token.State>>
//  ): Boolean {
//    spendLock.withLock {
//      val statement = services.jdbcSession().createStatement()
//      try {
//        statement.execute("CALL SET(@t, CAST(0 AS BIGINT));")
//
//        // we select spendable states irrespective of lock but prioritised by unlocked ones (Eg. null)
//        // the softLockReserve update will detect whether we try to lock states locked by others
//        val selectJoin = """
//            SELECT vs.transaction_id, vs.output_index, vs.contract_state, ct.AMOUNT, SET(@t, ifnull(@t,0)+ct.AMOUNT) total_pennies, vs.lock_id
//            FROM vault_states AS vs, CORDITE_TOKEN AS ct
//            WHERE vs.transaction_id = ct.transaction_id AND vs.output_index = ct.output_index
//            AND vs.state_status = 0
//            AND ct.TOKEN_TYPE_SYMBOL = '${amount.token.symbol}' and @t < ${amount.quantity}
//            AND ct.ACCOUNT_ID = '$accountId'
//            AND (vs.lock_id = '$lockId' OR vs.lock_id is null)          """ +
//
//            (if (notary != null)
//              " AND vs.notary_name = '${notary.name}'" else "") +
//            (if (onlyFromIssuerParties.isNotEmpty())
//              " AND ct.issuer_key IN ($issuerKeysStr)" else "") +
//            (if (withIssuerRefs.isNotEmpty())
//              " AND ccs.issuer_ref IN ($issuerRefsStr)" else "")
//
//        // Retrieve spendable state refs
//        val rs = statement.executeQuery(selectJoin)
//        log.debug(selectJoin)
//        var totalPennies = 0L
//        while (rs.next()) {
//          val txHash = SecureHash.parse(rs.getString(1))
//          val index = rs.getInt(2)
//          val stateRef = StateRef(txHash, index)
//          val state = rs.getBytes(3).deserialize<TransactionState<Token.State>>(context = SerializationDefaults.STORAGE_CONTEXT)
//          val pennies = rs.getLong(4)
//          totalPennies = rs.getLong(5)
//          val rowLockId = rs.getString(6)
//          stateAndRefs.add(StateAndRef(state, stateRef))
//          log.trace { "ROW: $rowLockId ($lockId): $stateRef : $pennies ($totalPennies)" }
//        }
//
//        if (stateAndRefs.isNotEmpty() && totalPennies >= amount.quantity) {
//          // we should have a minimum number of states to satisfy our selection `amount` criteria
//          log.trace("Coin selection for $amount retrieved ${stateAndRefs.count()} states totalling $totalPennies pennies: $stateAndRefs")
//
//          // With the current single threaded state machine available states are guaranteed to lock.
//          // TODO However, we will have to revisit these methods in the future multi-threaded. https://gitlab.com/cordite/cordite/issues/295
//          services.vaultService.softLockReserve(lockId, (stateAndRefs.map { it.ref }).toNonEmptySet())
//          return true
//        }
//        log.trace("Coin selection requested $amount but retrieved $totalPennies pennies with state refs: ${stateAndRefs.map { it.ref }}")
//        // retry as more states may become available
//      } catch (e: SQLException) {
//        log.error("""Failed retrieving unconsumed states for: amount [$amount], onlyFromIssuerParties [$onlyFromIssuerParties], notary [$notary], lockId [$lockId]
//                            $e.
//                        """)
//      } catch (e: StatesNotAvailableException) { // Should never happen with single threaded state machine
//        log.warn(e.message)
//        // retry only if there are locked states that may become available again (or consumed with change)
//      } finally {
//        statement.close()
//      }
//    }
//    return false
//  }
//}