/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dgl.corda.token

import io.cordite.dgl.corda.account.AccountAddress
import net.corda.core.contracts.*
import net.corda.core.identity.AbstractParty
import net.corda.core.schemas.MappedSchema
import net.corda.core.schemas.PersistentState
import net.corda.core.schemas.QueryableState
import net.corda.core.transactions.LedgerTransaction
import net.corda.core.utilities.toBase58String
import net.corda.core.utilities.toHexString
import java.security.PublicKey
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Index
import javax.persistence.Table

class Token : Contract {
  companion object {
    val CONTRACT_ID : ContractClassName = Token::class.java.name

    fun generateIssuance(amount: Amount<Issued<TokenType.Descriptor>>,
                         accountId: String,
                         owner: AbstractParty = amount.token.issuer.party,
                         description: String = ""): State {
      val exitKeys = listOf(amount.token.issuer.party.owningKey)
      return Token.State(amount = amount, exitKeys = exitKeys, owner = owner, accountId = accountId, description = description)
    }
  }

  override fun verify(tx: LedgerTransaction) {
    val command = tx.commands.requireSingleCommand<Command>()
    val groups = tx.groupStates(Token.State::class.java) { it.amount.token }
    for ((inputs, outputs, _) in groups) {
      when (command.value) {
        is Token.Command.Issue -> {
          requireThat {
            "There should be no inputs" using (inputs.isEmpty())
            "there are one or more outputs" using (outputs.isNotEmpty())

            outputs.forEach {
              "Issuer of the token is the same as the TokenType owner" using
                      (it.amount.token.issuer.party.nameOrNull() == it.amount.token.product.issuerName)
            }
          }
        }
        is Token.Command.Move -> {
          requireThat {
            "there are one or more inputs " using (inputs.isNotEmpty())
          }
          requireThat {
            "total input amount equals total output amount" using (inputs.map { it.amount.quantity }.sum() == outputs.map { it.amount.quantity }.sum())
          }
        }
//        is Token.Command.Settle -> {
//          requireThat { "there are one or more inputs" using (inputs.isNotEmpty()) }
//          requireThat { "there are zero outputs" using (outputs.isEmpty()) }
//          //TODO - we should checked that this is signed by the exit keys https://gitlab.com/cordite/cordite/issues/293
//        }
//        is Token.Command.Net -> {
//
//        }
      }
    }
  }

  open class State(override val amount: Amount<Issued<TokenType.Descriptor>>,
                   override val exitKeys: Collection<PublicKey>,
                   override val owner: AbstractParty,
                   val accountId: String,
                   val description: String)
    : FungibleAsset<TokenType.Descriptor>, QueryableState {

    override val participants : List<AbstractParty> = listOf(owner)
    open val contractId: ContractClassName get() = Token::class.java.name
    val accountAddress: AccountAddress = AccountAddress(accountId, owner.nameOrNull()!!)

    override fun withNewOwner(newOwner: AbstractParty)
        = CommandAndState(Token.Command.Move(), copy(owner = newOwner))

    override fun withNewOwnerAndAmount(
        newAmount: Amount<Issued<TokenType.Descriptor>>,
        newOwner: AbstractParty
    ): FungibleAsset<TokenType.Descriptor> {
      return copy(amount = amount.copy(newAmount.quantity), owner = newOwner)
    }

    open fun copy(amount: Amount<Issued<TokenType.Descriptor>> = this.amount,
                  exitKeys: Collection<PublicKey> = this.exitKeys,
                  owner: AbstractParty = this.owner,
                  participants: List<AbstractParty> = this.participants,
                  accountId: String = this.accountId): Token.State {
      return Token.State(amount, exitKeys, owner, accountId, description)
    }

    override fun generateMappedObject(schema: MappedSchema): PersistentState {
      return when (schema) {
        is TokenSchemaV1 -> {
          TokenSchemaV1.PersistedToken(
              accountId = accountId,
              description = description,
              amount = amount.quantity,
              tokenExponent = amount.token.product.exponent,
              tokenTypeSymbol = amount.token.product.symbol,
              issuer = amount.token.product.issuerName.toString(),
              issuerKey = amount.token.issuer.party.owningKey.toBase58String(),
              issuerRef = amount.token.issuer.reference.bytes.toHexString()
          )
        }
        else -> {
          throw IllegalArgumentException("Unrecognised schema $schema")
        }
      }
    }

    override fun supportedSchemas(): Iterable<MappedSchema> = listOf(TokenSchemaV1)
    override fun equals(other: Any?): Boolean {
      if (this === other) return true
      if (other !is State) return false

      if (amount != other.amount) return false
      if (exitKeys != other.exitKeys) return false
      if (owner != other.owner) return false
      if (accountId != other.accountId) return false
      if (description != other.description) return false
      if (participants != other.participants) return false
      if (accountAddress != other.accountAddress) return false

      return true
    }

    override fun hashCode(): Int {
      var result = amount.hashCode()
      result = 31 * result + exitKeys.hashCode()
      result = 31 * result + owner.hashCode()
      result = 31 * result + accountId.hashCode()
      result = 31 * result + description.hashCode()
      result = 31 * result + participants.hashCode()
      result = 31 * result + accountAddress.hashCode()
      return result
    }

  }

//  abstract class SettlementInstruction<out Destination>(
//      val destination: Destination,
//      val clientReference: String = "",
//      val clientDescription: String = "")

  /**
   * This is used for an on-ledger burn/exit of a token
   */
//  class BurnSettlementInstruction(clientReference: String,
//                                  clientDescription: String)
//    : SettlementInstruction<Unit>(Unit, clientReference, clientDescription)

  interface Command : CommandData {
    data class Move(override val contract: Class<out Contract>? = Token::class.java) : MoveCommand, Command
    class Issue : TypeOnlyCommandData(), Command
//    data class Net(val amounts: Map<Party, Amount<TokenType>>) : Command
//    data class Settle(val settlementInstruction: SettlementInstruction<*>) : Command
  }

  object TokenSchema

  object TokenSchemaV1 : MappedSchema(TokenSchema::class.java, 1, setOf(TokenSchemaV1.PersistedToken::class.java)
  ) {
    @Entity
    @Table(name = "CORDITE_TOKEN", indexes = arrayOf(
        Index(name = "account_idx", columnList = "account_id"),
        Index(name = "symbol_idx", columnList = "token_type_symbol"),
        Index(name = "issuer_idx", columnList = "issuer"),
        Index(name = "issuer_key_idx", columnList = "issuer_key"),
        Index(name = "issuer_ref_idx", columnList = "issuer_ref")
    ))
    class PersistedToken(
        @Column(name = "token_type_symbol")
        val tokenTypeSymbol: String,
        @Column(name = "amount")
        val amount: Long,
        @Column(name = "account_id")
        val accountId: String,
        @Column(name = "exponent")
        val tokenExponent: Int,
        @Column(name = "description")
        val description: String,
        @Column(name = "issuer")
        val issuer: String,
        @Column(name = "issuer_key")
        val issuerKey: String,
        @Column(name = "issuer_ref")
        val issuerRef: String
    ) : PersistentState()
  }
}


