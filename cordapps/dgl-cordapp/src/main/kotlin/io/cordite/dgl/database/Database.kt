/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
import rx.Observable
import rx.Subscriber
import java.sql.Connection
import java.sql.ResultSet

fun Connection.executeQuery(query: String): Observable<Map<String, Any>> {
  return Observable.create { subscriber ->
    this.prepareStatement(query).use { preparedStatement ->
      preparedStatement.executeQuery().use { rs ->
        rs.withSubscriber(subscriber)
      }
    }
  }
}

fun ResultSet.withSubscriber(subscriber: Subscriber<in Map<String, Any>>) {
  try {
    while (this.next()) {
      subscriber.onNext(this.toMap())
    }
    subscriber.onCompleted()
  } catch (err: Throwable) {
    subscriber.onError(err)
  }
}

fun ResultSet.toMap(): Map<String, Any> {
  return (1..metaData.columnCount).map { idx ->
    metaData.getColumnName(idx) to getObject(idx)
  }.toMap()
}