/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dao.core

import io.cordite.dao.membership.MemberProposal
import io.cordite.dao.proposal.ProposalContract
import io.cordite.dao.proposal.ProposalState
import net.corda.core.contracts.*
import net.corda.core.identity.Party
import net.corda.core.transactions.LedgerTransaction
import net.corda.core.transactions.TransactionBuilder

val DAO_CONTRACT_ID = "io.cordite.dao.core.DaoContract"

open class DaoContract : Contract {

  companion object {

    fun generateCreate(name: String, members: Set<Party>, initialModelData: Set<ModelData>, txBuilder: TransactionBuilder): DaoState {
      val newDaoState = DaoState(name, members).copyWith(initialModelData)
      val daoContractAndState = StateAndContract(newDaoState, DAO_CONTRACT_ID)
      val cmd = Command(Commands.CreateDao(), members.map{it.owningKey})

      txBuilder.withItems(daoContractAndState, cmd)

      return newDaoState
    }

    fun generateChangeMember(existingDaoTxStateAndRef: StateAndRef<DaoState>, changeMember: Party, proposalType: MemberProposal.Type, txBuilder: TransactionBuilder): Pair<DaoState, TransactionBuilder> {
      val existingDao = existingDaoTxStateAndRef.state.data

      val (outputDaoState, daoCommand) = when (proposalType) {
        MemberProposal.Type.NewMember -> getAddStateAndCommand(existingDao, changeMember)
        MemberProposal.Type.RemoveMember -> getRemoveStateAndCommand(existingDao, changeMember)
      }

      val outputDaoContractAndState = StateAndContract(outputDaoState, DAO_CONTRACT_ID)
      txBuilder.withItems(existingDaoTxStateAndRef, outputDaoContractAndState, daoCommand)

      return Pair(outputDaoState, txBuilder)
    }

    private fun getAddStateAndCommand(existingDao: DaoState, changeMember: Party): Pair<DaoState, Command<Commands.AddMember>> {
      if (existingDao.members.contains(changeMember)) {
        throw IllegalArgumentException("cannot add member to dao state again")
      }
      val existingMembershipModelData = existingDao.membershipModelData()

      var outputDaoState = existingDao.copyWith(changeMember)
      if (!existingMembershipModelData.hasMinNumberOfMembers && existingMembershipModelData.minimumMemberCount <= outputDaoState.members.size) {
        outputDaoState = outputDaoState.copyWith(existingMembershipModelData.copyWithMinMembers(true))
      }

      val daoCommand = Command(Commands.AddMember(changeMember), outputDaoState.members.map { it.owningKey })
      return Pair(outputDaoState, daoCommand)
    }

    private fun getRemoveStateAndCommand(existingDao: DaoState, changeMember: Party): Pair<DaoState, Command<Commands.RemoveMember>> {
      if (!existingDao.members.contains(changeMember)) {
        throw IllegalArgumentException("cannot remove member that doesn't exist")
      }
      val existingMembershipModelData = existingDao.membershipModelData()

      var outputDaoState = existingDao.copyWithout(changeMember)
      if (existingMembershipModelData.hasMinNumberOfMembers && existingMembershipModelData.minimumMemberCount > outputDaoState.members.size) {
        outputDaoState = outputDaoState.copyWith(existingMembershipModelData.copyWithMinMembers(false))
      }

      val daoCommand = Command(Commands.RemoveMember(changeMember), outputDaoState.members.map { it.owningKey })
      return Pair(outputDaoState, daoCommand)
    }

    fun generateChangeModelData(existingDaoTxStateAndRef: StateAndRef<DaoState>, acceptedProposalTxStateAndRef: StateAndRef<ProposalState<ModelDataProposal>>, notary: Party): Pair<DaoState, TransactionBuilder> {
      val existingDao = existingDaoTxStateAndRef.state.data
      val acceptedProposal = acceptedProposalTxStateAndRef.state.data

      val outputDaoState = existingDao.copyWith(acceptedProposal.proposal.newModelData)
      val outputDaoContractAndState = StateAndContract(outputDaoState, DAO_CONTRACT_ID)
      val daoCommand = Command(Commands.ChangeModelData(acceptedProposal.proposal.newModelData), outputDaoState.members.map { it.owningKey })

      val proposalCommand = Command(ProposalContract.Commands.ConsumeProposal(), acceptedProposal.participantSet().map { it.owningKey })

      val txBuilder = TransactionBuilder(notary = notary)
      txBuilder.withItems(acceptedProposalTxStateAndRef, proposalCommand, existingDaoTxStateAndRef, outputDaoContractAndState, daoCommand)

      return Pair(outputDaoState, txBuilder)
    }

    fun generateReferToDao(existingDaoTxStateAndRef: StateAndRef<DaoState>, txBuilder: TransactionBuilder): TransactionBuilder {
      val outputDaoState = existingDaoTxStateAndRef.state.data // do we need to create a dummy copy or can we get away with referring
      val outputDaoContractAndState = StateAndContract(outputDaoState, DAO_CONTRACT_ID)
      val cmd = Command(Commands.ReferToDao(), outputDaoState.members.map { it.owningKey })

      txBuilder.withItems(existingDaoTxStateAndRef, outputDaoContractAndState, cmd)

      return txBuilder
    }

  }

  private fun verifyModelDataTransition(oldDaoState: DaoState, newDaoState: DaoState, command: Commands) {
    newDaoState.modelDataMap.values.forEach {
      it.verify(oldDaoState, newDaoState, command)
    }
  }

  override fun verify(tx: LedgerTransaction) {
    val command = tx.commands.requireSingleCommand<Commands>()
    val groups = tx.groupStates { it: DaoState -> it.daoKey }

    for ((inputs, outputs, _) in groups) {

      // do we want to include removed members later? i think yes because otherwise someone can unilaterally remove everyone else

      when (command.value) {
        is Commands.CreateDao -> {
          requireThat {
            "no inputs should be consumed when creating a dao" using (inputs.isEmpty())
            "there should be one output state" using (outputs.size == 1)
            "exactly members must be signers" using (outputs.first().members.map { it.owningKey } == command.signers)
          }

        }

        is Commands.AddMember -> {
          requireThat {
            "there should be one input state" using (inputs.size == 1)
            "there should be one output state" using (outputs.size == 1)

            val inputState = inputs.first()
            val outputState = outputs.first()
            val newMember = (command.value as Commands.AddMember).newMember

            "no members should be removed" using (outputState.members.containsAll(inputState.members))
            "newMember should be in output members" using (outputState.members.contains(newMember))
            "newMember should not be in input members" using (!inputState.members.contains(newMember))
            "exactly members must be signers" using (outputs.first().members.map { it.owningKey } == command.signers)

            verifyModelDataTransition(inputState, outputState, command.value)
          }

        }

        is Commands.ReferToDao -> {
          requireThat {
            "there should be one input state" using (inputs.size == 1)
            "there should be one output state" using (outputs.size == 1)

            val inputState = inputs.first()
            val outputState = outputs.first()

            "the input and output state should be the same" using (inputState == outputState)
          }
        }

        is Commands.RemoveMember -> {
          requireThat {
            "there should be one input state" using (inputs.size == 1)
            "there should be one output state" using (outputs.size == 1)

            val inputState = inputs.first()
            val outputState = outputs.first()
            val removeMember = (command.value as Commands.RemoveMember).removeMember

            "inputState should contain all of the output state members" using (inputState.members.containsAll(outputState.members))
            "outputState should have one fewer members" using (inputState.members.size == outputState.members.size + 1)
            "removeMember should be in input members" using (inputState.members.contains(removeMember))
            "removeMember should not be in output members" using (!outputState.members.contains(removeMember))
            "exactly members must be signers" using (outputs.first().members.map { it.owningKey } == command.signers)

            verifyModelDataTransition(inputState, outputState, command.value)
          }
        }

        is Commands.ChangeModelData -> {
          requireThat {
            "there should be one input state" using (inputs.size == 1)
            "there should be one output state" using (outputs.size == 1)

            val inputState = inputs.first()
            val outputState = outputs.first()
            val newModelData = (command.value as Commands.ChangeModelData).newModelData

            "output state should have the new model data" using (outputState.get(newModelData::class) == newModelData)
            "input and output states should have same members" using (outputState.members == inputState.members)
            "exactly members must be signers" using (outputs.first().members.map { it.owningKey } == command.signers)

            verifyModelDataTransition(inputState, outputState, command.value)
          }
        }

        else -> throw IllegalArgumentException("unrecognised command: ${command.value}")
      }

    }

  }

  // Used to indicate the transaction's intent.
  interface Commands : CommandData {
    class CreateDao : TypeOnlyCommandData(), Commands
    class AddMember(val newMember: Party) : Commands
    class RemoveMember(val removeMember: Party) : Commands
    class ReferToDao : TypeOnlyCommandData(), Commands
    class ChangeModelData(val newModelData: ModelData) : Commands
  }

}