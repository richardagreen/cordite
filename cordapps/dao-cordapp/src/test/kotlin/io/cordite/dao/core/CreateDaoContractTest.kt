/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dao.core

import io.cordite.test.utils.daoKey
import io.cordite.test.utils.ledgerServices
import io.cordite.test.utils.proposerKey
import io.cordite.test.utils.proposerParty
import net.corda.core.identity.Party
import net.corda.testing.node.ledger
import org.junit.Test


class CreateDaoContractTest {

  private fun daoState(name: String = "dao", members: Set<Party> = setOf(proposerParty)) = DaoState(name, members)

  @Test
  fun `happy path should verify`() {
    ledgerServices.ledger {
      transaction {
        output(DAO_CONTRACT_ID, daoState())
        command(proposerKey.public, DaoContract.Commands.CreateDao())
        verifies()
      }
    }
  }

  @Test
  fun `propose should not consume state`() {
    ledgerServices.ledger {
      transaction {
        input(DAO_CONTRACT_ID, daoState())
        command(proposerKey.public, DaoContract.Commands.CreateDao())
        `fails with`("No inputs should be consumed when creating a dao")
      }
    }
  }

  @Test
  fun `there should be one output state`() {
    ledgerServices.ledger {
      transaction {
        val daoState = daoState()
        output(DAO_CONTRACT_ID, daoState)
        output(DAO_CONTRACT_ID, daoState)
        command(proposerKey.public, DaoContract.Commands.CreateDao())
        `fails with`("There should be one output state")
      }
    }
  }

  @Test
  fun `the proposer must be a signer`() {
    ledgerServices.ledger {
      transaction {
        output(DAO_CONTRACT_ID, daoState())
        command(daoKey.public, DaoContract.Commands.CreateDao())
        `fails with`("exactly members must be signers")
      }
    }
  }

}