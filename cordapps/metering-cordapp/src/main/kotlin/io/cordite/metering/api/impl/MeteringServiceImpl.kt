/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.metering.api.impl

import io.cordite.commons.utils.toVertxFuture
import io.cordite.commons.utils.transaction
import io.cordite.commons.utils.contextLogger
import io.cordite.metering.api.*
import io.cordite.metering.contract.MeteringInvoiceSplitState
import io.cordite.metering.contract.MeteringInvoiceState
import io.cordite.metering.contract.MeteringSplitState
import io.cordite.metering.contract.MeteringState
import io.cordite.metering.flow.*
import io.cordite.metering.schema.MeteringInvoiceSchemaV1
import io.cordite.metering.schema.MeteringInvoiceSplitSchemaV1
import io.cordite.metering.service.MeteringDaoStateLoader
import io.vertx.core.Future
import net.corda.core.contracts.StateAndRef
import net.corda.core.node.AppServiceHub
import net.corda.core.node.services.queryBy
import net.corda.core.node.services.vault.Builder.equal
import net.corda.core.node.services.vault.PageSpecification
import net.corda.core.node.services.vault.QueryCriteria


class MeteringServiceImpl(private val serviceHub: AppServiceHub) : MeteringApi {

  companion object {
    private val log = contextLogger()
  }

  private fun getMeteringInvoiceStates(meteringState: MeteringState?): List<MeteringInvoiceState> {
    val invoices = mutableListOf<StateAndRef<MeteringInvoiceState>>()

    serviceHub.transaction {

      val qc : QueryCriteria = QueryCriteria.VaultQueryCriteria(contractStateTypes = setOf(MeteringInvoiceState::class.java))
      var queryCriteria: QueryCriteria = qc

      if (meteringState != null) {
        val expression = MeteringInvoiceSchemaV1.PersistentMeteringInvoice::meteringState.equal(meteringState.name)
        val customCriteria = QueryCriteria.VaultCustomQueryCriteria(expression)
        queryCriteria = qc.and(customCriteria)
      }

      var pageNumber = 1  // pages start at one

      do {
        val pageSpec = PageSpecification(pageSize = 16, pageNumber = pageNumber)
        val results = serviceHub.vaultService.queryBy<MeteringInvoiceState>(queryCriteria, pageSpec)
        invoices.addAll(results.states)
        pageNumber++
      } while ((pageSpec.pageSize * (pageNumber)) <= results.totalStatesAvailable)

      log.info("meteringInvoiceStates: $invoices")

    }

    return invoices.map{it.state.data}
  }

  override fun listInvoices(meteringState: MeteringState?): List<MeteringInvoiceDetails> {
    return getMeteringInvoiceStates(meteringState).map { MeteringInvoiceDetails(it.meteringInvoiceProperties, it.owner.nameOrNull().toString()  ) }
  }

  override fun listInvoiceTxIds(meteringState: MeteringState?): List<String> {
    return getMeteringInvoiceStates(meteringState).map { it.meteringInvoiceProperties.meteredTransactionId }
  }

  override fun listInvoiceSplits(meteringSplitState: MeteringSplitState?): Future<List<MeteringInvoiceSplitDetails>> {

    val invoiceSplits = mutableListOf<StateAndRef<MeteringInvoiceSplitState>>()
    val f: Future<List<MeteringInvoiceSplitDetails>> = Future.future()

    try {
      serviceHub.transaction {

        val qc : QueryCriteria = QueryCriteria.VaultQueryCriteria(contractStateTypes = setOf(MeteringInvoiceSplitState::class.java))
        var queryCriteria: QueryCriteria = qc

        if (meteringSplitState != null) {
          val expression = MeteringInvoiceSplitSchemaV1.PersistentMeteringInvoiceSplit::meteringSplitState.equal(meteringSplitState.name)
          val customCriteria = QueryCriteria.VaultCustomQueryCriteria(expression)
          queryCriteria = qc.and(customCriteria)
        }

        var pageNumber = 1  // pages start at one

        do {
          val pageSpec = PageSpecification(pageSize = 16, pageNumber = pageNumber)
          val results = serviceHub.vaultService.queryBy<MeteringInvoiceSplitState>(queryCriteria, pageSpec)
          invoiceSplits.addAll(results.states)
          pageNumber++
        } while ((pageSpec.pageSize * (pageNumber)) <= results.totalStatesAvailable)

        log.info("meteringInvoiceSplitStates: $invoiceSplits")

        f.complete(invoiceSplits.map{ it -> MeteringInvoiceSplitDetails(it.state.data.meteringInvoiceSplitProperties, it.state.data.owner.nameOrNull().toString()  )  })

      }
    } catch (e: Exception) {
      e.printStackTrace()
      f.fail(e.message)
    }

    return f
  }


  override fun payInvoice(payRequest: MeteringInvoicePayRequest): Future<SimpleResult> {

    val f: Future<SimpleResult> = Future.future()

    val payMeteringInvoiceRequest = MeteringInvoiceFlowCommands.PayMeteringInvoiceRequest(payRequest.meteredTransactionId, payRequest.fromAccount)
    val payMeteringInvoiceFlow = PayMeteringInvoiceFlow.MeteringInvoicePayer(payMeteringInvoiceRequest = payMeteringInvoiceRequest)
    val payMeteringInvoiceFlowFuture = serviceHub.startFlow(payMeteringInvoiceFlow).returnValue

    // forward the async result to the Future<SimpleResult> that the client is waiting for
    payMeteringInvoiceFlowFuture.toVertxFuture().setHandler {
      h -> if( h.succeeded())
        f.complete(SimpleResult(true, "succeeded"))
      else
        f.complete(SimpleResult(false, h.cause().message))
    }

    return f
  }

    override fun payInvoices(payRequests: MeteringInvoicePayRequests): Future<SimpleResult> {
    val f: Future<SimpleResult> = Future.future()

    val requests: MutableList<MeteringInvoiceFlowCommands.PayMeteringInvoiceRequest> = mutableListOf()
    payRequests.meteredTransactionIds.forEach {
      val request = MeteringInvoiceFlowCommands.PayMeteringInvoiceRequest(meteredTransactionId = it,  fromAccount = payRequests.fromAccount)
      requests.add(request)
    }

    val payMeteringInvoicesFlow = PayMeteringInvoicesFlow.MeteringInvoicePayer(requests)
    val payMeteringInvoicesFlowFuture = serviceHub.startFlow(payMeteringInvoicesFlow).returnValue

    // forward the async result to the Future<SimpleResult> that the client is waiting for
    payMeteringInvoicesFlowFuture.toVertxFuture().setHandler {
      h -> if( h.succeeded())
      f.complete(SimpleResult(true, "succeeded"))
    else
      f.complete(SimpleResult(false, h.cause().message))
    }

    return f
  }


  override fun disputeInvoice(disputedInvoice: MeteringInvoice ): Future<SimpleResult> {

    val f: Future<SimpleResult> = Future.future()

    val disputeMeteringInvoiceRequest = MeteringInvoiceFlowCommands.DisputeMeteringInvoiceRequest(disputedInvoice.meteredTransactionId)
    val disputeMeteringInvoiceFlow = DisputeMeteringInvoiceFlow.MeteringInvoiceDisputer(disputeMeteringInvoiceRequest = disputeMeteringInvoiceRequest)
    val disputeMeteringInvoiceFlowFuture = serviceHub.startFlow(disputeMeteringInvoiceFlow).returnValue

    disputeMeteringInvoiceFlowFuture.toVertxFuture().setHandler {
      h -> if( h.succeeded())
      f.complete(SimpleResult(true, "succeeded"))
    else
      f.complete(SimpleResult(false, h.cause().message))
    }

    return f

  }

  override fun disputeInvoices(disputedInvoices: MeteringInvoices ): Future<SimpleResult> {

    val f: Future<SimpleResult> = Future.future()

    val requests: MutableList<MeteringInvoiceFlowCommands.DisputeMeteringInvoiceRequest> = mutableListOf()
    disputedInvoices.meteredTransactionIds.forEach {
      val request = MeteringInvoiceFlowCommands.DisputeMeteringInvoiceRequest(meteredTransactionId = it)
      requests.add(request)
    }

    val disputeMeteringInvoicesFlow = DisputeMeteringInvoicesFlow.MeteringInvoiceDisputer(disputeMeteringInvoiceRequests = requests)
    val disputeMeteringInvoicesFlowFuture = serviceHub.startFlow(disputeMeteringInvoicesFlow).returnValue

    disputeMeteringInvoicesFlowFuture.toVertxFuture().setHandler {
      h -> if( h.succeeded())
      f.complete(SimpleResult(true, "succeeded"))
    else
      f.complete(SimpleResult(false, h.cause().message))
    }

    return f

  }

  override fun reissueInvoice(invoiceToReissue: MeteringInvoiceReissueRequest): Future<SimpleResult> {

    val f: Future<SimpleResult> = Future.future()

    val reissueRequest = MeteringInvoiceFlowCommands.ReIssueMeteringInvoiceRequest(invoiceToReissue.meteredTransactionId, invoiceToReissue.tokenType, invoiceToReissue.amount)
    val reissueMeteringInvoiceFlow = ReissueMeteringInvoiceFlow.MeteringInvoiceReissuer(reissueMeteringInvoiceRequest = reissueRequest)
    val reissueMeteringInvoiceFlowFuture = serviceHub.startFlow(reissueMeteringInvoiceFlow).returnValue

    // forward the async result to the Future<SimpleResult> that the client is waiting for
    reissueMeteringInvoiceFlowFuture.toVertxFuture().setHandler {
      h -> if( h.succeeded())
      f.complete(SimpleResult(true, "succeeded"))
    else
      f.complete(SimpleResult(false, h.cause().message))
    }

    return f
  }


  override fun reissueInvoices(invoicesToReissue: MeteringInvoiceReissueRequests): Future<SimpleResult> {
    val f: Future<SimpleResult> = Future.future()

    val requests = mutableListOf<MeteringInvoiceFlowCommands.ReIssueMeteringInvoiceRequest>()
    invoicesToReissue.reissueRequests.forEach {
      val request = MeteringInvoiceFlowCommands.ReIssueMeteringInvoiceRequest(meteredTransactionId = it.meteredTransactionId, tokenDescriptor = it.tokenType, amount = it.amount)
      requests.add(request)
    }

    val reissueMeteringInvoicesFlow = ReissueMeteringInvoicesFlow.MeteringInvoiceReissuer( reissueMeteringInvoiceRequests = requests)
    val reissueMeteringInvoicesFlowFuture = serviceHub.startFlow(reissueMeteringInvoicesFlow).returnValue

    reissueMeteringInvoicesFlowFuture.toVertxFuture().setHandler {
      h -> if( h.succeeded())
      f.complete(SimpleResult(true, "succeeded"))
    else
      f.complete(SimpleResult(false, h.cause().message))
    }

    return f
  }

}

