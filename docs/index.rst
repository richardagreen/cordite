Cordite documentation!
======================


.. toctree::
   :hidden:
   :glob:

   Overview <content/overview>
   content/concepts
   Cordite Test <content/cordite_test>
   content/contributing
   FAQ <content/faq>

Cordite provides decentralised economics and governance services.
Cordite is regulatory friendly, enterprise ready and finance grade.
Cordite is an open source CorDapp, built on Corda.
