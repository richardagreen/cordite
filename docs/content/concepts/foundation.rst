Foundation
==========

The Cordite Foundation is, currently, a necessary thin legal wrapper
around Cordite, which is a DAO in Cordite.

The Foundation will be bootstrapped with a set of simple rules around:

-  membership - ie rules around joining
-  governance - who can make proposals, vote for proposals, what happens to accepted proposals
-  economics - how proceeds are used to run and evolve Cordite

Members of the Cordite Foundation are expected to:

-  have proof of stake
-  provide core infrastructure
-  make proposals for evolving all aspects of cordite

The aim of the foundation is to create a body of people who can be
trusted to over see the funding, running and evolution of cordite over
time. Much like an open source foundation.
